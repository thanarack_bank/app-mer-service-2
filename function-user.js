var mongodb = require('mongodb');
var ObjectID = mongodb.ObjectId;
var config = require('./config');

module.exports = {
    connectDB: function (mongod) {
        if (mongod === undefined) mongod = mongodb;
        return new Promise(function (resolve, reject) {
            mongod.connect(config.database_url, function (err, db) {
                if (err) return reject(err);
                else return resolve(db);
            });
        });
    },
    getUser: function (id) {
        var _connectDB = this.connectDB();
        var res = {};
        var param_id = new ObjectID(id);
        console.log(typeof param_id);
        return new Promise(function (resolve, reject) {
            _connectDB.then(function (db) {
                db.collection('users').findOne({_id: param_id}, {password: false}, function (err, result) {
                    if (err) return reject(err);
                    res.user = result;
                    res.db = db;
                    return resolve(res);
                });
            });
        });
    },
    checkUserExits: function (p_email, p_password) {
        var _connectDB = this.connectDB();
        var res = {};
        var obj = {};
        if (p_email) obj.email = p_email;
        if (p_password) obj.password = p_password;
        return new Promise(function (resolve, reject) {
            _connectDB.then(function (db) {
                db.collection('users').findOne(obj, function (err, result) {
                    if (err) return reject(err);
                    res.user = result;
                    res.db = db;
                    return resolve(res);
                });
            });
        });
    }
}
