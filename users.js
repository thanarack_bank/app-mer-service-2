var f_user = require('./function-user');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var func = require('./function');

module.exports = function (app) {

    app.get('/users/account', function (req, res) {
        f_user.connectDB().then(function (db) {
            db.collection('accounts').find().toArray(function (err, result) {
                console.log(result);
                if (result !== null) {
                    res.json({
                        status: 100,
                        data: result
                    })
                } else {
                    res.json({
                        status: 200,
                        message: "ไม่พบผู้ใช้งาน"
                    })
                }
            });
        });
    });

    app.post('/users', function (req, res) {
        if (req.body) {
            var obj = {
                name: req.body.name,
                lastName: req.body.lastName,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password),
                phone: req.body.phone,
                active: 1
            };
            f_user.checkUserExits(req.body.email).then(function (result) {
                if (result.user === null) {
                    result.db.collection('users').insertOne(obj, function (err, result) {
                        console.log(err)
                        if (result) {
                            res.json({
                                status: 100,
                                message: "เพิ่มข้อมูลผู้ใช้งานแล้ว",
                                data: result
                            });
                        }
                    });
                } else {
                    res.json({
                        status: 200,
                        message: "ผู้ใช้งานนี้มีอยู่ในระบบแล้ว"
                    });
                }
            }).catch(function (err) {
                res.json(err)
            });
        }
    });

    app.post('/users/facebook', function (req, res) {
        if (req.body) {
            var obj = {
                accName: req.body.name,
                token: req.body.accessToken,
                exp: req.body.expiresIn,
                idFacebook: req.body.idFacebook,
                idUser: req.body.idUser
            };
            f_user.connectDB().then(function (db) {
                db.collection('accounts').insertOne(obj, function (err, result) {
                    console.log(err)
                    if (result) {
                        res.json({
                            status: 100,
                            message: "เพิ่มบัญชีเฟสบุ็คแล้ว",
                            data: result
                        });
                    }
                });
            });
            /*f_user.checkUserExits(req.body.email).then(function (result) {
             if (result.user === null) {
             result.db.collection('account').insertOne(obj, function (err, result) {
             console.log(err)
             if (result) {
             res.json({
             status: 100,
             message: "เพิ่มข้อมูลผู้ใช้งานแล้ว",
             data: result
             });
             }
             });
             } else {
             res.json({
             status: 200,
             message: "ผู้ใช้งานนี้มีอยู่ในระบบแล้ว"
             });
             }
             }).catch(function (err) {
             res.json(err)
             });*/
        }
    });

    app.post('/users/login', function (req, res) {
        var obj = {
            email: req.body.email,
            password: req.body.password
        };

        if (!obj.email || !obj.password) {
            res.json({
                status: 200,
                message: "ไม่พบอีเมล์และรหัสผ่าน"
            });
        }

        f_user.checkUserExits(obj.email).then(function (result) {
            if (result.user !== null) {
                if (bcrypt.compareSync(obj.password, result.user.password)) {
                    var token = jwt.sign({data: obj.email}, app.get('app-secret'), {expiresIn: '24h'});
                    delete result.user['password'];
                    res.json({
                        status: 100,
                        message: "เข้าสู่ระบบสำเร็จ",
                        result: result.user,
                        token: token
                    });
                } else {
                    res.json({
                        status: 200,
                        message: "รหัสผ่านไม่ถูกต้อง"
                    });
                }
            } else {
                res.json({
                    status: 200,
                    message: "ไม่พบผู้ใช้งาน"
                });
            }
        });
    });

    app.get('/users/:id', func.middleWareToken, function (req, res) {
        f_user.getUser(req.params.id).then(function (result) {
            if (result.user !== null) {
                res.json({
                    status: 100,
                    data: result.user
                })
            } else {
                res.json({
                    status: 200,
                    message: "ไม่พบผู้ใช้งาน"
                })
            }
        });
    });


};

