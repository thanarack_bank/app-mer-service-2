var mongodb = require('mongodb');
var config = require('./config');
var jwt = require('jsonwebtoken');

module.exports = {
    middleWareToken: function (req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, config.token_secret, function (err, decode) {
                if (err) {
                    res.json({
                        status: 200,
                        message: "Token invalid."
                    })
                } else {
                    //console.log(decode);
                    req.decoded = decode;
                    next();
                }
            })
        } else {
            res.json({
                status: 200,
                message: "No token provided."
            });
        }
    }
}
