var config = {};
config.database_name = 'isell';
config.database_url = 'mongodb://localhost:27017/' + config.database_name;
config.token_secret = 'secret';
module.exports = config;