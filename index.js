var express = require('express');
var app = express();
var port = process.env.PORT || 3001;
var bodyParser = require('body-parser');
var config = require('./config');
var cors = require('cors');
var passport = require('passport');
var Strategy = require('passport-facebook').Strategy;

app.use(cors({origin: 'http://localhost:8080'}));
app.set('app-secret', config.token_secret);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

passport.use(new Strategy({
        clientID: '738039106380327',
        clientSecret: '4ca5c04ac7e293488ad38afd96f78419',
        callbackURL: "http://localhost:8080/settings/account"
    },
    function (accessToken, refreshToken, profile, cb) {
        User.findOrCreate({facebookId: profile.id}, function (err, user) {
            return cb(err, user);
        });
    }
));

app.get('/', function (req, res) {
    console.log(connectDB);
    res.send(connectDB + 'Hello world');
});

//app.post('/auth/facebook', passport.authenticate('facebook', {failureRedirect: '/login'}));

require('./users')(app);

console.log('Server is running at : ' + port);
app.listen(port);